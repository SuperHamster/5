#include "picture.h"

int main ()
{
	setlocale (LC_ALL, "Russian");

	cout << "\n\t\tКартинка №1 (Создаётся из файла picture.txt)\n\n";

	picture pic ("picture.txt");
	pic.pout ();
	pic.pout_r ();
	pic.pout_g ();
	pic.pout_b ();

	cout << "\n\t\t<---------------------------------------->\n\n\t\tКартинка №2 (Создаётся из файла picture2.txt)\n\n";

	picture poc ("picture2.txt");
	poc.pout ();
	poc.pout_r ();
	poc.pout_g ();
	poc.pout_b ();

	cout << "\n\t\t<---------------------------------------->\n\n\t\tКартинка №3 (С заданными размерами)\n\n";

	picture pac (4,3);
	pac.pout ();
	pac.pout_r ();
	pac.pout_g ();
	pac.pout_b ();

	cout << "\n\t\t<---------------------------------------->\n\n\t\tКартинка №4 (Создаётся по умолчанию)\n\n";

	picture pec;
	pec.pout ();
	pec.pout_r ();
	pec.pout_g ();
	pec.pout_b ();

	cout << "\n\t\t<---------------------------------------->\n\t\t\tРезультат суммирования:\n\n";
	
	pec.sum (pic, poc);

	pec.pout ();
	pec.pout_r ();
	pec.pout_g ();
	pec.pout_b ();

	system ("pause");
	return 0;
}