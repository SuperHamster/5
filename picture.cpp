#include "picture.h"

//Конструктор с параметрами
picture::picture (const string pfinp)
{
	ifstream in(pfinp);										
	if (!in.is_open())
	{
		cout << "\t\tФайл не может быть открыт\n\n" << endl;
		system ("pause");
		exit (-1);
	}
	for (n = 0;; n++)					//Высчитываем количество строк			
	{
		string str;
		getline(in, str);
		if (in.eof()) break;
	}
	in.clear();							
	in.seekg(0);
	for (k = 0;; k++)					//Высчитываем количество точек (пикселей)
	{
		string str1;
		getline(in, str1, '.');
		if (in.eof()) break;
	}
	k = k / n;								//Высчитываем количество столбцов
	rgb = new pixel *[n];				//Выделяем память под матрицу rgb размера n*k
	for (int c = 0; c < n; c++)
		rgb[c] = new pixel [k];
	in.clear();
	in.seekg(0);
	string str;
	for (int i = 0; i < n; i++)
	{
		getline(in, str);
		for(int j = 0; j < k; j++)
		{
			rgb[i][j].r = stoi(str.substr(0,3));		//Считывание значения для красной компоненты
			str.erase (0,4);
			rgb[i][j].g = stoi(str.substr(0,3));		//Считывание значения для зелёной компоненты
			str.erase (0,4);
			rgb[i][j].b = stoi(str.substr(0,3));		//Считывание значения для синей компоненты
			str.erase (0,4);
		}
	}
	in.close();
}

//Конструктор по умолчанию
picture::picture ()								//По умолчанию создаётся "картинка" 3х3 и заполняется нулями
{
	n = 3; k = 3;
	rgb = new pixel *[n]; 
	for (int c = 0; c < n; c++)
		rgb[c] = new pixel [k];
	for (int i = 0; i < n; i++)
	{
		for(int j = 0; j < k; j++)
		{
			rgb[i][j].r = 0;
			rgb[i][j].g = 0;
			rgb[i][j].b = 0;
		}
	}
}

//Конструктор картинки с заданным размером
picture::picture (int g, int h)
{
	n = g; k = h;
	rgb = new pixel *[n]; 
	for (int c = 0; c < n; c++)
		rgb[c] = new pixel [k];
	for (int i = 0; i < n; i++)
	{
		for(int j = 0; j < k; j++)
		{
			rgb[i][j].r = 0;
			rgb[i][j].g = 0;
			rgb[i][j].b = 0;
		}
	}
}

//Конструктор копирования
picture::picture (const picture &o)				//Выделение памяти и создание копии массива rgb
{
	rgb = new pixel *[o.n]; 
	for (int c = 0; c < o.n; c++)
		rgb[c] = new pixel [o.k];
	for (int x = 0; x < o.n; x++)
		for (int y = 0; y < o.k; y++)
		{
			rgb[x][y].r = o.rgb[x][y].r;
			rgb[x][y].g = o.rgb[x][y].g;
			rgb[x][y].b = o.rgb[x][y].b;
		}
}

//Деструктор
picture::~picture()					//Очистка выделеной памяти
{
	for( int i = 0; i < n; i++ )
		delete [] rgb[i];
	delete [] rgb;	
}

//Вывод
void picture::pout ()				//Вывод всех трёх компонент в формате (255 255 255)
{
	cout << "Матрица: " << endl;
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < k; j++)
		{
			cout << " (" << setw(3) << rgb[i][j].r << setw(4) << rgb[i][j].g << setw(4) << rgb[i][j].b << ") ";
		}
		cout << '\n';
	}
	cout << '\n';
}

//Вывод красной компоненты
void picture::pout_r ()
{
	cout << "Красная компонента: " << endl;
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < k; j++)
		{
			cout << " (" << setw(3) << rgb[i][j].r << ") ";
		}
		cout << '\n';
	}
	cout << '\n';
}

//Вывод зеленой компоненты
void picture::pout_g ()
{
	cout << "Зелёная компонента: " << endl;
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < k; j++)
		{
			cout << " (" << setw(3) << rgb[i][j].g << ") ";
		}
		cout << '\n';
	}
	cout << '\n';
}	

//Вывод синей компоненты
void picture::pout_b ()
{
	cout <<  "Синяя компонента: " << endl;
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < k; j++)
		{
			cout << " (" << setw(3) << rgb[i][j].b << ") ";
		}
		cout << '\n';
	}
	cout << '\n';
}

//Сложение
void picture::sum (picture pic, picture poc)			//Сложение соответствующих компонент массивов rgb, принадлежащих объектам pic и poc
{
	if ( (n != pic.n) || (n != poc.n) || (pic.n != poc.n) || (k != pic.k) || (k != poc.k) || (pic.k != poc.k))
	{
		cout << "\n\t\tСложение невозможно: картинки разного размера\n\n" << endl;
		system ("pause");
		exit (-2);
	}
	for (int i = 0; i < n; i++)
		for (int j = 0; j < k; j++)
		{
			rgb[i][j].r = pic.rgb[i][j].r + poc.rgb[i][j].r;	
			if (rgb[i][j].r > 255) 
				rgb[i][j].r = 255;

			rgb[i][j].g = pic.rgb[i][j].g + poc.rgb[i][j].g;
			if (rgb[i][j].g > 255) 
				rgb[i][j].g = 255;

			rgb[i][j].b = pic.rgb[i][j].b + poc.rgb[i][j].b;
			if (rgb[i][j].b > 255) 
				rgb[i][j].b = 255;
		}
}