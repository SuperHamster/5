#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>
#include <stdlib.h>

using namespace 
			std;

class picture 
{
private:
	struct pixel {		//Структура pixel
		int r,g,b;		//Красная, зелёная и синяя компоненты
	};
	pixel **rgb;		//Массив структур
	int n;				//количество столбцов
	int k;				//количество строк
	string pfinp;		// Файл данных
public:
	//Конструктор
	picture (const string pfinp);

	//Конструктор по умолчанию
	picture ();

	//Конструктор картинки с заданным размером
	picture (int g, int h);

	//Конструктор копирования
	picture (const picture &o);

	//Деструктор
	~picture ();

	//Вывод на консоль
	void pout ();

	//Вывод красной компоненты
	void pout_r ();

	//Вывод зеленой компоненты
	void pout_g ();

	//Вывод синей компоненты
	void pout_b ();

	//Сложение
	void sum (picture pic, picture poc);

};